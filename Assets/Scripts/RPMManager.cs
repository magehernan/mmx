using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MMX {
	public class RPMManager : MonoBehaviour {
		[SerializeField]
		private Slider slider;
		[SerializeField]
		private TextMeshProUGUI textRpm;
		[SerializeField]
		private Rotator rotartor;

		private float rpm = 0;
		private InputSystemMap inputSettings;
		private const float velocity = 80f;

		private void Awake() {
			slider.onValueChanged.AddListener(OnRPMChange);
			OnRPMChange(rpm);
			inputSettings = new();
			inputSettings.Rpm.Enable();
		}

		private void Update() {
			if (inputSettings.Rpm.Decrease.IsPressed()) {
				slider.value = rpm - velocity * Time.deltaTime;
			} else if (inputSettings.Rpm.Increase.IsPressed()) {
				slider.value = rpm + velocity * Time.deltaTime;
			}
		}

		private void OnRPMChange(float newRpm) {
			rpm = Mathf.Round(Mathf.Clamp(newRpm, 0f, 200f));
			textRpm.text = rpm.ToString("0");
			rotartor.SetRPM(-rpm);
		}
	}
}