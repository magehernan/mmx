using UnityEngine;

namespace MMX {
	public class ClockEscape : MonoBehaviour {
		[SerializeField]
		private Vector3 rotationMask;
		[SerializeField]
		private float rotatingSpeed;
		[SerializeField]
		private Vector3 rotationOffset;

		private Transform myTransform;

		private bool rotating = true;

		private void Awake() {
			myTransform = transform;
		}

		private void FixedUpdate() {
			if (rotating) {
				myTransform.Rotate(rotationMask * rotatingSpeed);
			}
		}

		private void OnTriggerEnter(Collider collision) {
			rotating = false;
		}

		private void OnTriggerExit(Collider other) {
			rotating = true;
		}

	}
}