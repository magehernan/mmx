﻿using TMPro;
using UnityEngine;

namespace MMX.ProgrammingWheel.Canvas {
	public class PinPositionTexts : MonoBehaviour {
		[SerializeField]
		private TextMeshProUGUI[] list;

		public void ChangePage(int page) {
			for (int i = 0; i < list.Length; i++) {
				list[i].text = ((i + 1) * page).ToString();
			}
		}
	}
}