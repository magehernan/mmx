using System;
using UnityEngine;
using UnityEngine.UI;

namespace MMX.ProgrammingWheel.Canvas {
	public class CanvasProgrammingWheel : MonoBehaviour {
		[SerializeField]
		private Button buttoToggleVisible;
		[SerializeField]
		private GameObject panel;
		[SerializeField]
		private ProgrammingWheelPins programmingWheelPins;

		[SerializeField]
		private PinPositionTexts pinPositionTexts;

		[SerializeField]
		private PinsControl[] pinsControls;

		private bool isVisible = true;

		private int currentPage = 1;

		private void Awake() {
			buttoToggleVisible.onClick.AddListener(OnClickToggleVisible);
			OnClickToggleVisible();
		}

		private void OnClickToggleVisible() {
			isVisible = !isVisible;
			panel.SetActive(isVisible);
			if (isVisible) { 
				LoadPage(currentPage);
			}
		}

		private void LoadPage(int page) {
			currentPage = page;
			pinPositionTexts.ChangePage(page);


			//foreach (PinsControl pinControl in pinsControls) {
			//	for (int i = 0; i < 16; i++) {
			//		pinControl.SetPin(i, )
			//		programmingWheelPins.
			//	}
			//}
		}
	}
}