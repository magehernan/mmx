﻿using TMPro;
using UnityEngine;

namespace MMX.ProgrammingWheel.Canvas {
	public class PinPositionNumerator : MonoBehaviour {

		private void OnValidate() {
			TextMeshProUGUI[] list = transform.GetComponentsInChildren<TextMeshProUGUI>();
			for (int i = 0; i < list.Length; i++) {
				list[i].text = (i + 1).ToString();
			}
		}
	}
}