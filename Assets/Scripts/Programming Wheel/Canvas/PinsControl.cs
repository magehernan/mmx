﻿using UnityEngine;
using UnityEngine.UI;

namespace MMX.ProgrammingWheel.Canvas {
	public class PinsControl : MonoBehaviour {
		[SerializeField]
		private Toggle[] list;

		private void Awake() {
			for (int i = 0; i < list.Length; i++) {
				int position = i;
				list[i].onValueChanged.AddListener(active => { OnChangePin(position, active); });
			}
		}

		private void OnChangePin(int position, bool active) {

		}

		public void SetPin(int position, bool active) {
			list[position].isOn = active;
		}

		//private void OnValidate() {
		//	list = transform.GetComponentsInChildren<Toggle>();
		//}
	}
}