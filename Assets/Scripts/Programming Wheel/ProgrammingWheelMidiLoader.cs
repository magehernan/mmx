using B83.Win32;
using MPTK.NAudio.Midi;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace MMX.ProgrammingWheel {
	public class ProgrammingWheelMidiLoader : MonoBehaviour {
		[SerializeField]
		private TextAsset webMidi;
		[SerializeField]
		private ProgrammingWheelPins programmingWheelPins;

		private void Start() {
#if UNITY_WEBGL
			byte[] data = webMidi.bytes;
#else
			byte[] data = File.ReadAllBytes($"{Application.streamingAssetsPath}/tracks.midi");
#endif
			Load(data);
		}

		private void Load(byte[] data) {
			programmingWheelPins.ClearPins();


			MidiFile midiLoad = new(data, false);
			for (int i = 0; i < midiLoad.Tracks; i++) {
				IList<MidiEvent> track = midiLoad.Events[i];
				int trackNumber = -1;
				foreach (MidiEvent midiEvent in track) {
					//Debug.Log($"{midiEvent.GetAsShortMessage()}");
					if (midiEvent.CommandCode == MidiCommandCode.MetaEvent && midiEvent is TextEvent meta) {
						Debug.Log(meta.Text);
						trackNumber = meta.Text.ToUpper() switch {
							"VIBRAPHONE" => 0,
							"DRUM KIT" => 1,
							"BASS" => 2,
							_ => -1
						};
					}

					if (trackNumber == -1) {
						break;
					}

					if (midiEvent.CommandCode == MidiCommandCode.NoteOn && midiEvent is NoteEvent note) {
						int position = (int)(note.AbsoluteTime / (float)midiLoad.DeltaTicksPerQuarterNote * 4f);
						programmingWheelPins.SetPin(note.NoteName, position, trackNumber);
					}

				}
			}
		}

		void OnEnable() {
			// must be installed on the main thread to get the right thread id.
			UnityDragAndDropHook.InstallHook();
			UnityDragAndDropHook.OnDroppedFiles += OnFiles;
		}
		void OnDisable() {
			UnityDragAndDropHook.UninstallHook();
		}

		void OnFiles(List<string> aFiles, POINT aPos) {
			if (aFiles.Count > 1 || aFiles.Count == 0) {
				return;
			}

			byte[] data = File.ReadAllBytes(aFiles[0]);
			Load(data);
		}
	}
}
