﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MMX.ProgrammingWheel {
	public class ProgrammingWheelPins : MonoBehaviour {
		private const float CHANNEL_DISTANCE = 0.021f;
		private const int CHANNELS = 38;

		[SerializeField]
		private PinLane pinLanePrefab;
		[SerializeField]
		private List<PinLane> pinLanes;

		private class ChannelState {
			public int number;
			public int startPosition = -1;
		}

		private static readonly Dictionary<string, ChannelState>[] noteToChannel = new Dictionary<string, ChannelState>[] {
			new() {
				{ "B5", new() { number = 0 } },
				{ "C6", new() { number = 2 } },
				{ "D6", new() { number = 4 } },
				{ "E6", new() { number = 6 } },
				{ "F#6", new() { number = 8 } },
				{ "G6", new() { number = 10 } },
				{ "A6", new() { number = 12 } },
				{ "B6", new() { number = 14 } },
				{ "C7", new() { number = 16 } },
				{ "D7", new() { number = 18 } },
				{ "E7", new() { number = 20 } },
			},
			new() {
				{ "Bass Drum 1", new() { number = 24 } },
				{ "Acoustic Snare", new() { number = 26 } },
				{ "Crash Cymbal 2", new() { number = 28 } },
			},
			new() {
			}
		};

		private readonly List<int> channelNotes = new();

		public void ClearPins() {
			channelNotes.Clear();
			channelNotes.AddRange(Enumerable.Range(0, CHANNELS / 2));
			foreach (PinLane pinLane in pinLanes) {
				pinLane.ClearPins();

			}
		}

		public void SetPin(string note, int position, int track) {
			Debug.Log($"track: {track} note: {note}");
			if (!noteToChannel[track].TryGetValue(note, out ChannelState channel)) {
				return;
			}

			if (position - channel.startPosition > 255) {
				return;
			}
			if (channel.startPosition == -1) {
				channel.startPosition = position;
			}
			int noteChannel = channel.number / 2;
			int count = channelNotes[noteChannel];
			int newChannel = channel.number + count % 2;

			if (position > 255) {
				position -= 255;
			}

			pinLanes[newChannel].SetPin(position);
			channelNotes[noteChannel]++;
		}


		private void OnValidate() {
			for (int i = 0; i < CHANNELS; i++) {
				if (pinLanes.Count <= i) {
					pinLanes.Add(null);
				}

				PinLane pinLane = pinLanes[i];
				if (pinLanes[i] == null) {
					pinLane = Instantiate(pinLanePrefab, transform, false);
					pinLanes[i] = pinLane;
				}

				pinLane.name = $"Pin Lane {i}";
				pinLane.transform.localPosition = new Vector3(i * -CHANNEL_DISTANCE, 0f, 0f);
				pinLane.transform.SetSiblingIndex(i);
			}

			if (transform.childCount > CHANNELS) {
				Debug.LogError("THERE ARE MORE LANES THAN CHANNELS, REMOVE THE EXTRA LANES", this);
			}
		}
	}
}