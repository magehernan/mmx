using System;
using UnityEngine;

namespace MMX.ProgrammingWheel {
	public class PinLane : MonoBehaviour {
		private const int PIN_AMOUNT = 64 * 4;
		private const float LANE_ANGLE = 360f;
		private const float PIN_ANGLE = LANE_ANGLE / PIN_AMOUNT;
		[SerializeField]
		private GameObject pinPrefab;

		public void ClearPins() {
			for (int i = 0; i < transform.childCount; i++) {
				Destroy(transform.GetChild(i).gameObject);
			}
		}

		public void SetPin(int position) {
			if(position < 0 || PIN_AMOUNT <= position) {
				throw new ArgumentOutOfRangeException($"Pin position is out of range, 0..{PIN_AMOUNT} value: {position}");
			}

			GameObject go = Instantiate(pinPrefab, transform, false);
			go.name = $"Pin {position}";
			go.transform.localEulerAngles = new Vector3(position * PIN_ANGLE, 0, 0);
		}

		public bool IsPinActive(int position) {
			return transform.GetChild(position).gameObject.activeSelf;
		}
	}
}