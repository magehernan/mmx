﻿using UnityEngine;

namespace MMX {
	public class MarbleSpawner : MonoBehaviour {
		[SerializeField]
		private GameObject marblePrefab;
		[SerializeField]
		private GameObject[] points;

		[SerializeField]
		private float spawnSpeed = 1f;

		public void Spawn() {
			for (int i = 0; i < points.Length; i++) {
				Instantiate(marblePrefab, points[i].transform.position, Quaternion.identity);
			}
		}
	}
}