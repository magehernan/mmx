﻿using UnityEngine;

namespace MMX {
	public class Axis : MonoBehaviour {
		[SerializeField]
		private Vector3 axisRotation;

		[SerializeField]
		private Gear[] gears;

		private Transform myTransform;

		private void Awake() {
			myTransform = transform;
		}

		public void Rotate(float originRotation) {
			myTransform.Rotate(axisRotation * originRotation);

			foreach (Gear gear in gears) {
				gear.AxisRotate(originRotation);
			}
		}
	}
}