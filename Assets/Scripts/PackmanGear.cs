using UnityEngine;

namespace MMX {
	public class PackmanGear : MonoBehaviour {
		[SerializeField]
		private Transform packmanTransform;
		[SerializeField]
		private float rate;
		[SerializeField]
		private AxisTypes axis;

		private Transform myTransform;

		private Vector3 initialRotation;

		private void Awake() {
			myTransform = transform;
			initialRotation = myTransform.localEulerAngles;
		}

		private void FixedUpdate() {
			int currentAxis = (int)axis;
			Vector3 rotation = Vector3.zero;
			float delta = Mathf.DeltaAngle(myTransform.localEulerAngles[currentAxis], initialRotation[currentAxis]);
			rotation[currentAxis] = delta * rate;
			packmanTransform.localEulerAngles = rotation;
		}
	}
}