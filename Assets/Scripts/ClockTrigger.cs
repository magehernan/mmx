using UnityEngine;

namespace MMX {
	public class ClockTrigger : MonoBehaviour {
		[SerializeField]
		private Transform triggerTransform;
		[SerializeField]
		private float openPosition;

		private Transform myTransform;

		private Vector3 triggerInitialPosition;

		private void Awake() {
			myTransform = transform;
			triggerInitialPosition = triggerTransform.position;
		}

		private void FixedUpdate() {
			float delta = triggerInitialPosition.z - triggerTransform.position.z;

			if (delta < openPosition) {
				delta = openPosition;
			}

			myTransform.localPosition = new Vector3(0, delta, 0);
		}
	}
}