using UnityEngine;

namespace MMX {
	public class MarbleLift : MonoBehaviour {
		private const string Marble = "Marble";
		[SerializeField]
		private bool onEnter;
		[SerializeField]
		private float positionX;
		[SerializeField]
		private bool onExit;


		private void OnTriggerEnter(Collider other) {
			if (!onEnter) {
				return;
			}

			if (other.CompareTag(Marble)) {
				other.attachedRigidbody.useGravity = false;
				other.attachedRigidbody.constraints = RigidbodyConstraints.FreezePositionX;
				Vector3 position = other.transform.position;
				position.x = positionX;
				other.transform.position = position;
			}
		}

		private void OnTriggerExit(Collider other) {
			if (!onExit) {
				return;
			}

			if (other.CompareTag(Marble)) {
				other.attachedRigidbody.useGravity = true;
				other.attachedRigidbody.constraints = RigidbodyConstraints.None;
			}
		}
	}
}