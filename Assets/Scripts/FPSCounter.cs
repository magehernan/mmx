using TMPro;
using UnityEngine;

public class FPSCounter : MonoBehaviour {
	private TextMeshProUGUI text;

	private int fps;
	private float updateTime;

	private void Awake() => text = GetComponent<TextMeshProUGUI>();

	private void Update() {
		if (!text.enabled) {
			return;
		}

		fps++;
		updateTime += Time.unscaledDeltaTime;

		if (updateTime >= 1f) {
			updateTime = 0f;
			text.text = fps.ToString();
			fps = 0;
		}
	}
}