using UnityEngine;

namespace MMX {
	public class ChangeVelocityRigidbody : MonoBehaviour {
		[SerializeField]
		private bool setMinVelocity = true;
		[SerializeField]
		private float minVelocity = 0f;
		[SerializeField]
		private float maxVelocity = 99999f;
		[SerializeField]
		private float changeVelocityFactor;

		private void OnTriggerStay(Collider other) {
			ChangeVelocity(other);
		}

		private void ChangeVelocity(Collider other) {
			Vector3 velocity = other.attachedRigidbody.linearVelocity;
			float magnitude = velocity.magnitude;
			float newMagniture = magnitude + magnitude * changeVelocityFactor * Time.deltaTime;
			if (newMagniture < minVelocity) {
				if (!setMinVelocity) {
					return;
				}
				newMagniture = minVelocity;
			}

			if (newMagniture > maxVelocity) {
				newMagniture = maxVelocity;
			}

			other.attachedRigidbody.linearVelocity = velocity.normalized * newMagniture;
		}
	}
}