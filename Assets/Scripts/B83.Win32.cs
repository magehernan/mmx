using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace B83.Win32 {
	public enum HookType : int {
		WH_GETMESSAGE = 3,
	}

	// windows messages
	public enum WM : uint {
		NULL = 0x0000,
		DROPFILES = 0x0233,
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct POINT {
		public int x, y;
		public POINT(int aX, int aY) {
			x = aX;
			y = aY;
		}
		public override string ToString() {
			return "(" + x + ", " + y + ")";
		}
	}

	//WH_GETMESSAGE
	[StructLayout(LayoutKind.Sequential)]
	public struct MSG {
		public IntPtr hwnd;
		public WM message;
		public IntPtr wParam;
		public IntPtr lParam;
		public ushort time;
		public POINT pt;
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct RECT {
		public int Left, Top, Right, Bottom;

		public RECT(int left, int top, int right, int bottom) {
			Left = left;
			Top = top;
			Right = right;
			Bottom = bottom;
		}
		public override string ToString() {
			return "(" + Left + ", " + Top + ", " + Right + ", " + Bottom + ")";
		}
	}

	public delegate IntPtr HookProc(int code, IntPtr wParam, ref MSG lParam);
	public delegate bool EnumThreadDelegate(IntPtr Hwnd, IntPtr lParam);

#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN

	public static class Window {
		[DllImport("user32.dll")]
		public static extern bool EnumThreadWindows(uint dwThreadId, EnumThreadDelegate lpfn, IntPtr lParam);

		[DllImport("user32.dll")]
		public static extern bool IsWindowVisible(IntPtr hWnd);

		[DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
		static extern int GetClassName(IntPtr hWnd, System.Text.StringBuilder lpClassName, int nMaxCount);
		public static string GetClassName(IntPtr hWnd) {
			System.Text.StringBuilder sb = new(256);
			int count = GetClassName(hWnd, sb, 256);
			return sb.ToString(0, count);
		}
	}

	public static class WinAPI {
		[DllImport("kernel32.dll", CharSet = CharSet.Auto)]
		public static extern IntPtr GetModuleHandle(string lpModuleName);
		[DllImport("kernel32.dll")]
		public static extern uint GetCurrentThreadId();

		[DllImport("user32.dll", SetLastError = true)]
		public static extern IntPtr SetWindowsHookEx(HookType hookType, HookProc lpfn, IntPtr hMod, uint dwThreadId);
		[DllImport("user32.dll", SetLastError = true)]
		public static extern bool UnhookWindowsHookEx(IntPtr hhk);
		[DllImport("user32.dll")]
		public static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, ref MSG lParam);

		[DllImport("shell32.dll")]
		public static extern void DragAcceptFiles(IntPtr hwnd, bool fAccept);
		[DllImport("shell32.dll", CharSet = CharSet.Unicode)]
		public static extern uint DragQueryFile(IntPtr hDrop, uint iFile, System.Text.StringBuilder lpszFile, uint cch);
		[DllImport("shell32.dll")]
		public static extern void DragFinish(IntPtr hDrop);

		[DllImport("shell32.dll")]
		public static extern void DragQueryPoint(IntPtr hDrop, out POINT pos);
	}
#endif


	public static class UnityDragAndDropHook {
		public delegate void DroppedFilesEvent(List<string> aPathNames, POINT aDropPoint);
		public static event DroppedFilesEvent OnDroppedFiles;

#if UNITY_STANDALONE_WIN && !UNITY_EDITOR_WIN

		private static uint threadId;
		private static IntPtr mainWindow = IntPtr.Zero;
		private static IntPtr m_Hook;
		private static string m_ClassName = "UnityWndClass";

		// attribute required for IL2CPP, also has to be a static method
		[AOT.MonoPInvokeCallback(typeof(EnumThreadDelegate))]
		private static bool EnumCallback(IntPtr W, IntPtr _) {
			if (Window.IsWindowVisible(W) && (mainWindow == IntPtr.Zero || (m_ClassName != null && Window.GetClassName(W) == m_ClassName))) {
				mainWindow = W;
			}
			return true;
		}

		public static void InstallHook() {
			threadId = WinAPI.GetCurrentThreadId();
			if (threadId > 0) {
				Window.EnumThreadWindows(threadId, EnumCallback, IntPtr.Zero);
			}

			IntPtr hModule = WinAPI.GetModuleHandle(null);
			m_Hook = WinAPI.SetWindowsHookEx(HookType.WH_GETMESSAGE, Callback, hModule, threadId);
			// Allow dragging of files onto the main window. generates the WM_DROPFILES message
			WinAPI.DragAcceptFiles(mainWindow, true);
		}
		public static void UninstallHook() {
			WinAPI.UnhookWindowsHookEx(m_Hook);
			WinAPI.DragAcceptFiles(mainWindow, false);
			m_Hook = IntPtr.Zero;
		}

		// attribute required for IL2CPP, also has to be a static method
		[AOT.MonoPInvokeCallback(typeof(HookProc))]
		private static IntPtr Callback(int code, IntPtr wParam, ref MSG lParam) {
			if (code == 0 && lParam.message == WM.DROPFILES) {
				WinAPI.DragQueryPoint(lParam.wParam, out POINT pos);

				// 0xFFFFFFFF as index makes the method return the number of files
				uint n = WinAPI.DragQueryFile(lParam.wParam, 0xFFFFFFFF, null, 0);
				System.Text.StringBuilder sb = new(1024);

				List<string> result = new();
				for (uint i = 0; i < n; i++) {
					int len = (int)WinAPI.DragQueryFile(lParam.wParam, i, sb, 1024);
					result.Add(sb.ToString(0, len));
					sb.Length = 0;
				}
				WinAPI.DragFinish(lParam.wParam);
				OnDroppedFiles?.Invoke(result, pos);
			}
			return WinAPI.CallNextHookEx(m_Hook, code, wParam, ref lParam);
		}
#else
		public static void InstallHook() {
		}
		public static void UninstallHook() {
		}
#endif
	}
}
