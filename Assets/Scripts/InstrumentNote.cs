using UnityEngine;

namespace MMX {
	public class InstrumentNote : MonoBehaviour {
		[SerializeField]
		private AudioSource audioSource;
		[SerializeField]
		private AudioClip audioClip;

		private void OnCollisionEnter(Collision collision) {
			audioSource.PlayOneShot(audioClip);
		}
	}
}