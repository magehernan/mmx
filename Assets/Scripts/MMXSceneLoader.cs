using UnityEngine;
using UnityEngine.SceneManagement;

namespace MMX {
	public class MMXSceneLoader : MonoBehaviour {
		private void Start() {
#if UNITY_EDITOR
			for (int i = 0; i < SceneManager.sceneCount; i++) {
				if (SceneManager.GetSceneAt(i).name.Equals("MMX Scene")) {
					return;
				}
			}
#endif
			SceneManager.LoadScene("Scenes/MMX Scene", LoadSceneMode.Additive);
		}
	}
}