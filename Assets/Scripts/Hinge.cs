using UnityEngine;

namespace MMX {
	public class Hinge : MonoBehaviour {
		[SerializeField]
		private Optional<Transform> originTransform;
		[SerializeField]
		private Optional<Transform> centerRotation;
		[SerializeField]
		private AxisTypes axis;
		[SerializeField]
		private Vector3 rotationMask;
		[SerializeField]
		private Transform extremeTransform;
		[SerializeField]
		private Optional<Vector3> eulerRotation;
		[SerializeField]
		private bool active = true;

		private Transform myTransform;

		private void Awake() {
			myTransform = transform;
		}

		private void FixedUpdate() {
			if (!active) {
				return;
			}

			if (originTransform) {
				myTransform.position = originTransform.Value.position;
			}

			Vector3 position = myTransform.position;
			if (!centerRotation) {
				return;
			}

			float magnitude = (extremeTransform.position - position).magnitude;
			int currentAxis = (int)axis;
			float angle = Mathf.Acos((position[currentAxis] - centerRotation.Value.position[currentAxis]) / magnitude) * Mathf.Rad2Deg;

			if (float.IsNaN(angle)) {
				return;
			}

			Vector3 newRotation = -rotationMask * angle;
			if (eulerRotation) {
				newRotation = eulerRotation + newRotation;
			}
			myTransform.localEulerAngles = newRotation;
		}
	}
}