using UnityEngine;

namespace MMX {
	public class Spring : MonoBehaviour {
		[SerializeField]
		private Transform originTransform;
		[SerializeField]
		private AxisTypes axis;
		private Transform myTransform;

		private float initialSize;

		private void Awake() {
			myTransform = transform;
			initialSize = (originTransform.position - myTransform.position).magnitude;
		}

		private void FixedUpdate() {

			Vector3 size = new(1f, 1f, 1f);
			size[(int)axis] = (originTransform.position - myTransform.position).magnitude / initialSize;
			myTransform.localScale = size;
		}
	}
}